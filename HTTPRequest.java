package javasql;
import java.net.*;
import java.io.*;
import java.util.*;

public class HTTPRequest
{
	public String httpVersion;
	public String url;
	public String method;
	public String body;
	public HashMap<String,String> headers;

	HTTPRequest()
	{

	}
	HTTPRequest(String requeststring)
	{
		String[] requestLines = requeststring.split("\n");
		String[] requestattrs = requestLines[0].split(" ");
		this.httpVersion = requestattrs[2];
		this.url = requestattrs[1];
		this.method = requestattrs[0];
		this.headers = new HashMap<String,String>();
		int i=1;
		for(i=1;i<requestLines.length && requestLines[i] != "\n";i++)
		{
			String[] Entry = requestLines[i].split(": ");
			if(Entry.length > 1)
			this.headers.put(Entry[0],Entry[1]);
			else
				break;
		}
		this.body = "";
		for(;i<requestLines.length;i++)
		{
			this.body += requestLines[i] + "\n"; 
		}
	}
	public Route getRoute()
	{
		Route r = new Route();
		r.method = new String(this.method);
		String[] urlparse = this.url.split("[?]");
		r.path = new String(urlparse[0]);
		if( urlparse.length > 1)
		{
			String paramstring = urlparse[1];
			String[] params = paramstring.split("&");
			for(String param : params)
			{
				String[] entry = param.split("=");
				r.params.put(entry[0],entry[1]);
			} 
		}
		if(this.method.equals("POST"))
		{
			String data = new String(this.headers.get("Content-Type"));
			//System.out.println("data" + data);
			String[] temp = data.split("; ");
			data = temp[1];
			temp = data.split("=");
			String boundary = temp[1];
			String[] entries = body.split(boundary + "\n");
			for(String entry : entries)
			{
				String[] lines = entry.split("\n");
				if(lines.length > 0)
				{
					String key = new String(" ");
					temp = lines[0].split("; ");
					if(temp.length > 1)
					{
						data = temp[1];
						temp = data.split("=");
						key = new String(temp[1]);
						String val = new String("");
						val = lines[2];
						r.bodyparams.put(key,val);	
					}
				}
			}

		}
		//System.out.println(r.bodyparams);
		return r;
	}

} 