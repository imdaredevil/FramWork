package javasql;
import java.net.*;
import java.io.*;
import java.util.*;


public class Server{
	public static void main(String args[])
	{
		ServerSocket serverSock;
		try
		{
		 	serverSock= new ServerSocket(8000);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}	
		while(true)
		{
			try
			{
				Socket clientSock = serverSock.accept();
				DataInputStream requestStream = new DataInputStream(clientSock.getInputStream());
				Scanner cin = new Scanner(System.in);
				PrintStream responseStream = new PrintStream(clientSock.getOutputStream());
				String temp;
				temp = requestStream.readLine();
				if(temp != null)
				{
				String requestString = new String(temp);
				requestString += "\n";
				while(requestStream.available() > 0)
				{
					System.out.println(temp);
					temp = requestStream.readLine();
					requestString += temp + "\n";
				}

				HTTPRequest request = new HTTPRequest(requestString);
				Route route = request.getRoute();
				String today = route.handle();
				String test="HTTP/1.1 200 OK\r\n\r\n" + today;
				clientSock.getOutputStream().write(test.getBytes());
				}
				clientSock.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

	}
}