package javasql;
import java.net.*;
import java.io.*;
import java.util.*;

public class Route
{
	public String method;
	public String path;
	public HashMap<String,String> params;
	public HashMap<String,String> bodyparams;

	public Route()
	{
		this.method = new String();
		this.path = new String();
		this.params = new HashMap<String,String>();
		this.bodyparams = new HashMap<String,String>();
	}
	Route(Route rout)
	{
		this.method = new String(rout.method);
		this.path = new String(rout.path);
		this.params = new HashMap<String,String>(rout.params);
		this.bodyparams = new HashMap<String,String>(rout.bodyparams);
	}

	private String getHTML(String filename) throws FileNotFoundException,IOException
		{
		FileInputStream in = new FileInputStream("javasql/templates/" + filename);
		Scanner cin = new Scanner(in);
		String response = new String("");
		while(cin.hasNext())
			response = response + cin.next() + "\n";
	//	System.out.println(response);
		in.close();
		return response;
		}

	public String handle() throws FileNotFoundException,IOException
	{
		System.out.println(this.method + " " + this.path);
		
		if(this.path.equals("/") && this.method.equals("GET"))
		{
			return getHTML("Login_form.html");
		}

		if(this.path.equals("/") && this.method.equals("POST"))
		{
			SQLHelper sql = new SQLHelper("cibi","password");
			System.out.println(this.bodyparams);
			Boolean correctness = sql.checklogin(this.bodyparams.get("\"" + "uname" + "\"" ),this.bodyparams.get("\"" + "password" + "\""));
			if(correctness)
				return getHTML("sql.html");
			else
				return "wrong credentials";
		}

		if(this.path.equals("/query") && this.method.equals("POST"))
		{
			SQLHelper sql = new SQLHelper("cibi","password");
			return sql.execute(this.bodyparams.get("\"" + "query" + "\"" ),this.bodyparams.get("\"" + "type" + "\""));
		}

		return "no route error 404";

	}
	
}